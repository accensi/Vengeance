Vengeance is a personal mod that has been in development for many years, even changed names a few times, and acted as a playground for DECORATE and ZScript stuff.

**INSTALLATION:** Download the zip and load it into GZDoom. No need to change its extension to .pk3. However, a .pk3 extension is required when loading the mod with Delta Touch.

**Weapons**

*  Vindicator - Highly advanced assault rifle, capable of utilizing several ammo types. Use the Previous/Next Weapon keys to navigate and Fire to select ammo type. Works best with mouse scroll wheel. Almost all ammo types have an alt-fire. Those that don't can have their primary attack comboed.
*  Sword - Fallback weapon. Used as a quick attack for Vindicator. Activated by using the Zoom key. Damage scales with player velocity. Can be comboed with Blink's buff for even more damage.

**Skills**

*  Frailty - Offensive skill. A homing projectile that curses enemes, causing them to die when low on health, and also spawns a new seeker upon their death to curse nearby enemies. Can be chained indefinitely.
*  Vengeance - Offensive skill. The "ultimate" ability. Increases damage done, reduces damage taken, increases speed, regenerates health, and frightens enemies for a short duration.
*  Blink - Support skill. Allows for instantaneous movement. Time is frozen while Blink is active, but you only have a short amount of time before it auto-activates! Your movement is also suspended as long as no movement keys are pressed. Same for frozen time. Blinking leaves behind a shadow that makes all enemies and their tracers target it instead of you. Also gives you a melee damage boost as long as the shadow exists.
*  Ignore Pain - Support skill. Reduces all incoming damage to a certain amount for a short duration. Any additional damage above that amount is ignored.

**Resources**
*  Health - If you lose it, you die. Hatred slowly regenerates it. Higher amount of Hatred regenerates health faster.
*  Hatred - Fuels offensive skills. Replenish by executing enemies or getting damaged. Passively goes down with time.
*  Discipline - Fuels support skills. Replenish by killing enemies using Vindicator. Passively goes down with time, though at a slower rate than Hatred.
*  Ammo - You need to fight for ammo. Execute enemies to replenish it.

**Items**
*  Extended Backpack - Gives a bit of every ammo type. Has a chance to give a Core as well.
*  Berserk - Restores some health (can overheal) and increases Sword damage for a short duration. Also gives a temporary speed boost and damage resistance. While under the effects of this power-up, Hatred directly influences the amount of melee damage. Velocity and Blink buffs are also stacked with this for maximum carnage.
*  Soulsphere - Glorified portable medikit. Can restore up to 100% of missing health. Only restores what's missing upon usage, leaving the rest as reserve.
*  Ultrasphere - Restores 200% of health.
*  Quad Damage - Quake style.

**Other Abilities**
*  Double jumping. Press Jump while in the air to double-jump. It transfers momentum towards the jump angle, allowing you to make sharp turns around corners or change direction mid-air. Takes a bit of usage to master.
*  Dodging. Press Crouch while moving in any direction to dodge in that direction. Works just like double jumping in terms of physics. Costs a bit of Discipline, because it's a combat ability. To crouch normally, press the key when not moving. To uncrouch, let go of the key and stop moving.
*  Executions. Press Zoom while aiming at an enemy that's low on health to teleport to it and instantly kill it. An indicator will appear above the enemy when you are within range and can perform the attack.