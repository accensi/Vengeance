//=========================
// VINDICATOR
//=========================

material texture "Models/Weapons/VindicatorSkin_d.png"
{
	normal "Models/Weapons/VindicatorSkin_n.png"
	specular "Models/Weapons/VindicatorSkin_s.png"
	brightmap "Models/Weapons/VindicatorSkin_b.png"
	specularlevel 2
}

material texture "Models/Weapons/VindicatorSkinRocket_d.png"
{
	normal "Models/Weapons/VindicatorSkin_n.png"
	specular "Models/Weapons/VindicatorSkin_s.png"
	brightmap "Models/Weapons/VindicatorSkin_b.png"
	specularlevel 2
}

material texture "Models/Weapons/VindicatorSkinCore_d.png"
{
	normal "Models/Weapons/VindicatorSkin_n.png"
	specular "Models/Weapons/VindicatorSkin_s.png"
	brightmap "Models/Weapons/VindicatorSkin_b.png"
	specularlevel 2
}

material texture "Models/Weapons/VindicatorSkinEmpty_d.png"
{
	normal "Models/Weapons/VindicatorSkin_n.png"
	specular "Models/Weapons/VindicatorSkin_s.png"
	brightmap "Models/Weapons/VindicatorSkin_b.png"
	specularlevel 2
}

material texture "Models/Weapons/HandsSkin_d.png"
{
	normal "Models/Weapons/HandsSkin_n.png"
	specular "Models/Weapons/HandsSkin_s.png"
	brightmap "Models/Weapons/HandsSkin_b.png"
	specularlevel 2
}

material texture "Models/Weapons/SwordSkin_d.png"
{
	normal "Models/Weapons/SwordSkin_n.png"
	specular "Models/Weapons/SwordSkin_s.png"
	brightmap "Models/Weapons/SwordSkin_b.png"
	specularlevel 2
}

PointLight VindicatorRocketLight
{
	Color 0.7 0.7 0.1
	Size 48
}

Object VindicatorRocketStandard { Frame HAMM { Light VindicatorRocketLight } }

PointLight ExplosionLight
{
	Color 0.6 0.6 0.1
	Size 64
}

PointLight PlasmaLightBlue
{
	color 0.4 0.5 7.0
	size 48
}

Object VindicatorTracerPlasma { Frame TRCR { Light PlasmaLightBlue } }

PulseLight ImpactBlue
{
	color 0.1 0.25 0.5
	size 48
	secondarySize 16
	interval 1.5
}

PointLight ImpactGreenSmall
{
	color 0.6 1.0 0.0
	size 128
}

PointLight ImpactGreenBig
{
	color 0.6 1.0 0.0
	size 256
}

Object VindicatorImpactPlasma { Frame PLHT { Light ImpactBlue } }
Object VindicatorRayImpact { Frame TNT1 { Light ImpactGreenSmall } }
Object VindicatorOrbImpact { Frame TNT1 { Light ImpactGreenBig } }

Object VindicatorOrb { Frame TNT1 { Light VindicatorOrb } }

PointLight VindicatorOrb
{
	Color 0.3 0.8 0.1
	Size 192
}

PointLight DeathCoreLight
{
	Color 0.3 0.8 0.1
	Size 32
}

//=========================
// SKILLS
//=========================

//------------- BLINK -------------

HardwareShader PostProcess scene
{
	Name "ZoomShader"
	Shader "Shaders/ZoomShader.txt" 330
	Uniform float factor
}

HardwareShader PostProcess scene
{
	Name "BlurShader"
	Shader "Shaders/BlurShader.txt" 330
	Uniform float factor
}

PointLight BlinkGlow
{
	Color 0.20 0.15 0.45
	Size 48
}

//------------- FRAILTY -------------

PointLight FrailtyRuneGlow
{
	Color 0.4 0 0.8
	Size 128
}

PointLight FrailtyIndicatorGlow
{
	Color 0.6 0.6 0.6
	Size 64
}

PointLight FrailtySeekerGlow
{
	Color 0.4 0 0.8
	Size 32
}

Object FrailtyRune { Frame FRLG { Light FrailtyRuneGlow } }
Object FrailtyIndicator { Frame FRLG { Light FrailtyIndicatorGlow } }
Object FrailtySeeker { Frame TNT1 { Light FrailtySeekerGlow } }

//=========================
// QUAD DAMAGE
//=========================
PointLight QuadDamageLight
{
	Color 0.2 0.1 0.8
	Size 32
}

//=========================
// SOULSPHERE
//=========================
PulseLight SOULSPHERE
{
	color 0.2 0.4 1.0
	size 40
	secondarySize 32
	interval 1.0
}

Object VSoulSphere
{
	Frame SOUL { Light SOULSPHERE }
}

//=========================
// ULTRASPHERE
//=========================
PulseLight UltraSphere
{
	color 0.3 0.3 0.3
	size 40
	secondarySize 32
	interval 2.0
}